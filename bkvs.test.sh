#!/bin/sh

OLDDIR=$BKVS_ROOTDIR
unset BKVS_ROOTDIR

hash bkvs 2>&- || { echo >&2 'Could not find bkvs. FAIL!'; exit 1; }
bkvs >/dev/null 2>&1 || { echo >&2 'bkvs exited with probable syntax error. FAIL!'; exit 1; }
[[ -d /tmp/bkvs ]] || { echo >&2 'bkvs tmp dir not created. FAIL!'; exit 1; }
export BKVS_ROOTDIR="./tmp"
bkvs set hello world
[[ -f $BKVS_ROOTDIR/hello ]] || { echo >&2 'bkvs set failed. FAIL!'; exit 1; }
cat $BKVS_ROOTDIR/hello | fgrep world >/dev/null 2>&1
[[ 0 -eq $? ]] || { echo >&2 'Correct value not stored. FAIL!'; exit 1; }
bkvs get hello | fgrep world >/dev/null 2>&1
[[ 0 -eq $? ]] || { echo >&2 'bkvs get returned wrong value. FAIL!'; exit 1; }
bkvs delete hello
[[ -e $BKVS_ROOTDIR/hello ]] && { echo >&2 'bkvs delete failed (file still exists). FAIL!'; exit 1; }
bkvs delete
[[ -e $BKVS_ROOTDIR ]] && { echo >&2 'bkvs delete failed (db still exists). FAIL!'; exit 1; }

echo "All pass."

export BKVS_ROOTDIR=$OLDDIR
